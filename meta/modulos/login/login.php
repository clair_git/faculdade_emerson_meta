<?php 
require_once('../../classes/autoload.php');

$login = $_POST['login'];
$senha = $_POST['senha'];

$autoriza = $users->autenticacao($login,$senha);

if($autoriza){

	$_SESSION['id_grupo'] = $autoriza->fk_grupo;
	$_SESSION['log'] = true;
	redirect('../index.php');
}else{
	redirect('index.php');
}