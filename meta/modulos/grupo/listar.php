<?php 
require_once("../classes/autoload.php")
$grupo_all = $grupos->all();

 ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <!-- left column -->
                    <!-- /.card-header -->
                    <div class="card-body table-responsive " >
                      <table class="table table-head-fixed text-nowrap">
                        <thead>
                          <tr>
                            <th>descricao</th>                           
                            <th>Ações</th>
                          </tr>
                        </thead>
                        <tbody>


                          <?php 
                            foreach($grupo_all as $grupo){
                           ?>


                          <tr>
                            <td><?php echo $grupo->descricao ?></td>
                            <td>
                              <a href="index.php?modulo=grupo&acao=editar&id=<?php echo $grupo->id?>"><button class='btn btn-primary'></button></a>
                              <a href="index.php?modulo=grupo&acao=deletar&id=<?php echo $grupo->id?>"><button class='btn btn-danger'></button></a>
                            </td>
                          </tr>

                          <?php 
                        }
                           ?>
                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
                </div>
              </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  
</div>
<!-- ./wrapper -->
