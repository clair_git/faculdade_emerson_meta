<?php 
require_once("../classes/autoload.php")
$modulo_all = $modulos->all();

 ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <!-- left column -->
                    <!-- /.card-header -->
                    <div class="card-body table-responsive " >
                      <table class="table table-head-fixed text-nowrap">
                        <thead>
                          <tr>
                            <th>descrição</th>
                            <th>caminho</th>
                            <th>Ações</th>
                          </tr>
                        </thead>
                        <tbody>


                          <?php 
                            foreach($modulo_all as $modulo){
                           ?>


                          <tr>
                            <td><?php echo $modulo->descricao ?></td>
                            <td><?php echo $modulo->diretorio ?></td>
                            <td>
                              <a href="index.php?modulo=modulo&acao=editar&id=<?php echo $modulo->id?>"><button class='btn btn-primary'></button></a>
                              <a href="index.php?modulo=modulo&acao=deletar&id=<?php echo $modulo->id?>"><button class='btn btn-danger'></button></a>
                            </td>
                          </tr>

                          <?php 
                        }
                           ?>
                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
                </div>
              </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  
</div>
<!-- ./wrapper -->
