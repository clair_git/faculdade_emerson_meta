<?php
require_once('model.php');

class Operacao extends Model{
  
 protected $table='operacoes';
 protected $columns = [
  'id',
  'fk_grupo',
 'fk_modulo'
 ];
  protected $fillable = [
  'fk_grupo',
 'fk_modulo'
 ];


public function modulo(){
    return $this->belongsTo('modulo',$this->fk_modulo);
 }
 public function verificaPermissao($idgrupo,$modulo){
    $content = $this->where('fk_grupo','=',$idgrupo)->get();
    foreach($content as $rown){
        if($rown->modulo()->diretorio == $modulo){
            return true;
        }
    }
    return false;
 }
}
 