<?php
require_once('model.php');

class Modulo extends Model{
  
 protected $table='modulos';
 protected $columns = [
  'id',
  'descricao',
 'diretorio'
 ];
  protected $fillable = [
  'descricao',
 'diretorio'
 ];
}
 