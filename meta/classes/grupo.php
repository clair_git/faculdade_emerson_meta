<?php
require_once('model.php');

class Grupo extends Model{
  
 protected $table='grupos';
 protected $columns = [
  'id',
  'descricao'
 ];
  protected $fillable = [
  'descricao'
 ];
}
 